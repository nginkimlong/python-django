
arr = {}
# {'student A': [90, 76, 50, 80, 100]}
def find_avg(s1, s2, s3, s4, s5): 
    avg = (s1 + s2 + s3 + s4 +s5) / 5
    return avg 
def result(avg): 
    if avg >=50: 
        if avg >=90 and avg <= 100: 
            print("Grade A")
        elif avg >=80 and avg < 90: 
            print("Grade B")
        elif avg >=70 and avg < 80:
            print("Grade C")
        elif avg >= 60 and avg < 70:
            print("Grade D")
        else: 
            print("Grade E")
    else:
        print("Grade F")
while True: 
    name = input("Input student name: ")
    s1 = input("Input score 1: ")
    s2 = input("Input score 2: ")
    s3 = input("Input score 3: ")
    s4 = input("Input score 4: ")
    s5 = input("Input score 5: ")
    arr[name] = [s1, s2, s3, s4, s5]
    x = input("Do you want to continue?y/n: ")
    if x == 'y':
        continue
    else: 
        break
for k, v in arr.items(): 
    print("Student name is: ", k)
    avg = find_avg(int(v[0]), int(v[1]), int(v[2]), int(v[3]), int(v[4]))
    result(avg)
    print("==========================")