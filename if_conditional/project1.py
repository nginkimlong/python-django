# passed, false 

arr_stu = {}

# {"Student A": [90, 80, 100], "Student B": [], "Student C": []}

while True: 
    name = input("Student name: ")
    s1 = float(input("Input score 1: "))
    s2 = float(input("Input score 2: "))
    s3 = float(input("Input score 3: "))
    arr_stu[name] = [s1, s2, s3]
    x = input("Yes/ No: ")
    if x == "yes": 
        continue
    else: 
        break


for k, v in arr_stu.items(): 
    avg = (v[0] + v[1] + v[2]) / 3
    print("Student : ", k, " AVG: ", avg)
    # A (90 - 100), B (80-89), C (70-79), D(60-69), E(50-59), F(<50)
    
    # ============= First method ===========
    # if avg >= 90 and avg <= 100:
    #     print("Grade A")
    # if avg >= 80 and avg <90: 
    #     print("Grade B")
    # if avg >= 70 and avg < 80:
    #     print("Grade C")
    # if avg >=60 and avg < 70: 
    #     print("Grade D")
    # if avg >=50 and avg < 60: 
    #     print("Grade E")
    # if avg < 50: 
    #     print("Grade F")

    # ========== Second method =============
    if avg >= 50:
        if avg >= 90 and avg <= 100:
            # A+ (96-100)
            # A- (93-95)
            # A
            if avg >= 96 and avg <= 100:
                print("Grade A+")
            elif avg >= 93 and avg <96:
                print("Grade A-")
            else: 
                print("Grade A")
        elif avg >= 80 and avg <90: 
            print("Grade B")
        elif avg >= 70 and avg < 80:
            print("Grade C")
        elif avg >=60 and avg < 70: 
            print("Grade D")
        else: 
            print("Grade E")
    else: 
        print("Grade F")



    # if avg > 50: 
    #     print("Passed")
    # else: 
    #     print("False")

