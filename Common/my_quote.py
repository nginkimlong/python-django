age = '12'
name = "Hello world"
sex = """Female"""
print(type(age))
print(type(name))
print(type(sex))

print("==========================")
# This block of code show about various types of variables type, 
# including have: integer, boolean, float also
age= 28 #integer type 
status  = True # status type
salary = 1200.56  # float type
print(type(age))
print(type(status))
print(type(salary))

