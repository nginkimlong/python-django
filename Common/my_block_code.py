# Block in condition expression
x = int(input("Input value: "))
if x%2==0: 
    print("Even")
    for i in range(0, x): 
        print("Hello world: ",i) 
        print("Thank you")
        x = x + 1  
else: 
    print("Odd")

# Block in Loop statement 
x = ["Student A", "Student B", "Student C", "Student D", "Student E"]
for i in x: 
    print(i)

# Block in Function statement 
def getAVG(s1, s2, s3): 
    avg = (s1 + s2 + s3) / 3 
    return avg 

result = getAVG(30, 40, 50)
print("Average is: ", result)

# Block in class 
class Person: 
    def __init__(self, name, age, sex, status):
        this.name = name 
        this.age = age 
        this.sex = sex 
        this.status = status
    
    def show_detail_person(self): 
        print("Student name is: ", this.name, \
            " Sex: ", this.sex, " Age: ", \
            this.age, " Status: ", this.status)

obj = Person("Chheng Koing", 19, "M", True)
obj.show_detail_person()


