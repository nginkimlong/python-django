class Person: 
    def __init__(self, first_name, last_name, sex, age):
        self.f_name = first_name
        self.l_name = last_name 
        self.sex = sex 
        self.age = age 
class Job(Person):
    def __init__(self, first_name, last_name, sex, age):
        super().__init__(first_name, last_name, sex, age)

obj  = Person("Chamnab", "Ray", "Male", 21)
print("Full name : ", obj.f_name, " ", obj.l_name)