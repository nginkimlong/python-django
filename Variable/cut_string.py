from itertools import count


str = "Great explorer technology center"

# Using find() function 
find_str = str.find("explorer")
print("Find result is: ", find_str)


print(str[0]) # Start getting index of string (0)
print(len(str)) # Count number of character

# Syntax: [start_index: end_index]  GET character from Start_index to End_index
get_index= str[1: 10]
print("Filter character: ", get_index)



# HOMEWORK get value from behind
# my_str = str[-32:-3]
# print(my_str)



# Replace all behind 
# new_str = str[:6]+ "Hello world"
# print(new_str)

# Replace specific string

new_str = str.replace("center", "institute")
print(new_str)