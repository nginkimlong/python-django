# Initialize list item 
list_student1 = ["Student A", "Student B", "Student C", "Student D", "Student E"]
list_student1.sort() # Ascending sort 
list_student1.sort(reverse=True) # Descending sor

print("After ascending sort: ", list_student1)

list_student2 = [1, 2, 3]
# list_student2.extend(list_student1) # First combine method
# new_list = list_student2 + list_student1 # Second combine method
# print(new_list)



print(list_student1)
# Filter value 
find_student = list_student1[4]
print("Find student with index 4 is: ", find_student)
# Slice Student 
slice_student = list_student1[1: 4] 
print("Slice student : ", slice_student)

# Update student 
list_student1[1] = "Student BB"
print("After update: ", list_student1)

# Delete list item
del list_student1[1]
print("After delete: ", list_student1)
del list_student1[0:2]
print("After delete multiple item: ", list_student1)

# Using list() function 
_title = list("Hell world")
print(_title)



# Declaration list
# list_student = [] 
# while True: 
#     student_name = input("Student name: ")
