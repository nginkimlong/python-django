dic_student = {
    'A': "Student A", 
    'B': "Student B", 
    'C': "Student C",
    'D': "Student D",
    'E': "Student E",
}
# print(type(dic_student))
r1 = dic_student.get("D")
print(r1)

dic_student["G"] = "Student G"
print(dic_student)

# Delete item from dictionary 
del dic_student['G']
print("After delete: ", dic_student)

# Update key 
dic_student["EE"] = dic_student.pop('E')
dic_student["EE"] = "New Student E"
print("After update:", dic_student)

# Delete item by using pop() function
del_student = dic_student.pop("EE")
print("DELTE STUDENT: ", del_student)
print("After delete: ", dic_student)
# Get all keys only

print(dic_student.keys())
# GET all values only
print(dic_student.values())

# GET keys and values both
print(dic_student.items())

# Filter all items in dictionary as interations
for k, v in dic_student.items(): 
    print("Key is: ", k, "value is: ", v)







