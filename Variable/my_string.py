# str = "greatx center"
# str = 'greatx center'
str = """ Greatx Center """
print(str)
# Using strip()
print(str.strip())

# using upper case 
print("UPPER: ", str.upper())
# Lower case 
print("lower", str.lower())
# Swapcase 
print("sWAP", str.swapcase())
# capitalize sentences 
print("Capitalize: ", str.capitalize())
str = """ greatx center """
# Title 
print("Title: ", str.title())





